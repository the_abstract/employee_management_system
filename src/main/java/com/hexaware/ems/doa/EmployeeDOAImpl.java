package com.hexaware.ems.doa;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;
import java.util.Set;
import java.util.HashSet;

import com.hexaware.ems.model.Employee;
import com.hexaware.ems.util.JDBCUtil;

public class EmployeeDOAImpl implements EmployeeDAO {

	private final JDBCUtil jdbcUtil;

	public EmployeeDOAImpl(JDBCUtil jdbcUtil) {
		this.jdbcUtil = jdbcUtil;
	}

	public Employee save(Employee employee) {
		Connection connection = null;
		try {
			connection = jdbcUtil.getDBConnection("root", "Password123");
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("SELECT * FROM Employee");
			if (resultSet.next()) {
				int employeeId = resultSet.getInt(1);
				String empName = resultSet.getString(2);
				LocalDate dateOfJoining = resultSet.getDate(3).toLocalDate();
				String email = resultSet.getString(4);
				Employee manager = findById(resultSet.getInt(5));

				Employee employee3 = new Employee(employeeId, empName, dateOfJoining, email);
				return employee3;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {

		}
		return null;
	}

	public Set<Employee> findAll() {
		Connection connection = null;
		
		Set<Employee> set = new HashSet<Employee>();
		try {
		connection = jdbcUtil.getDBConnection("root", "Password123");
		Statement statement = connection.createStatement();
		ResultSet resultSet = statement.executeQuery("SELECT * FROM Employee");

		while(resultSet.next()) {
				int employeeId = resultSet.getInt(1);
		        String empName = resultSet.getString(2);
		        LocalDate dateOfJoining = resultSet.getDate(3).toLocalDate();
		        String email = resultSet.getString(4);
		        Employee manager = findById(resultSet.getInt(5));
		        Employee employee= new Employee(employeeId, empName, dateOfJoining, email);
		        set.add(employee);
		        }
		}catch (SQLException e) {
		e.printStackTrace();
		}
		               
		   return set;
	}

	public Employee findById(long employeeId) {
		Connection connection = null;
		try {
			connection = jdbcUtil.getDBConnection("root", "Password123");
			PreparedStatement prepareStatement = connection.prepareStatement("SELECT * from Employee where emp_id = ?");

			prepareStatement.setLong(1, employeeId);
			// prepareStatement.setString(2, "johnny");

			ResultSet resultSet = prepareStatement.executeQuery();
			if (resultSet.next()) {
				long i = resultSet.getLong(1);
				String name = resultSet.getString(2);
				LocalDate date = resultSet.getDate(3).toLocalDate();
				String email = resultSet.getString(4);
				Employee manager = findById(resultSet.getInt(5));

				ResultSet resultSet2 = prepareStatement
						.executeQuery("select * from Employee where emp_id = manager_id");
				Employee manager1 = new Employee(employeeId, name, date, email);
				manager1.setId(manager.getId());
				// long manager_id = manager1.getId();
				manager1.setName(resultSet2.getString(2));
				manager1.setEmail(resultSet2.getString(3));
				manager1.setDateOfJoining(resultSet2.getDate(4).toLocalDate());

				Employee employee = new Employee(i, name, date, email);
//				manager1.setId(manager.getId());

				return employee;
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {

		}

		return null;
	}

	public void deleteById(long employeeId) {
		// TODO Auto-generated method stub

	}

}
