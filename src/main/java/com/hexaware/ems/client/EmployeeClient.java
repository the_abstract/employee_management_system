 package com.hexaware.ems.client;

import java.time.LocalDate;

import com.hexaware.ems.doa.EmployeeDAO;
import com.hexaware.ems.doa.EmployeeDOAImpl;
import com.hexaware.ems.model.Employee;
import com.hexaware.ems.service.EmployeeService;
import com.hexaware.ems.service.EmployeeServiceImpl;
import com.hexaware.ems.util.JDBCUtil;

public class EmployeeClient {
	
	
	public static void main(String[] args) {
		//Employee em = new Employee(1010, "sameer", LocalDate.of(2019, 12, 12), "sameer@gmail.com", em);
		JDBCUtil jdbc = new JDBCUtil();
		EmployeeDAO empDAO = new EmployeeDOAImpl(jdbc);
		Employee findById = empDAO.findById(1010l);
		System.out.println(findById);
		
	}
	
	
	
	
}
