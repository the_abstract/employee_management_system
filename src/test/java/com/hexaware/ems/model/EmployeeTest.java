package com.hexaware.ems.model;

import java.time.LocalDate;

import org.junit.Assert;
import org.junit.Test;

import com.hexaware.ems.model.Employee;

public class EmployeeTest {
	
	@Test
	public void empIdTest() {
		Employee empId = new Employee(1000, "Len", LocalDate.of(2020, 05, 25), "len@lenovo.com");
		empId.setId(1000);
		Assert.assertEquals(1000, empId.getId());
	}
	
	@Test
	public void empName() {
		
		Employee empName = new Employee(1500, "Kong", LocalDate.of(2020, 05, 15), "kong@kongland.com");
		empName.setName("Kong");
		Assert.assertEquals("Kong", empName.getName());
	} 
	
	@Test
	public void empDateOfJoining() {
		Employee empDateOfJoining = new Employee(1000, "MiA3", LocalDate.of(2019, 05, 15), "miA3@mi.com");
		
		Assert.assertEquals(LocalDate.of(2019, 06, 15), empDateOfJoining.getDateOfJoining());
	}
}
